package com.leanteq.stringcalculator;

import org.assertj.core.api.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringCalculatorTest {

    StringCalculator calculator;

    @BeforeEach
    public void setUp(){
        calculator = new StringCalculator();
    }

    // Step 1 Test cases
    @Test
    public void shouldReturnZero() {
        // Given
        String number = "";
        // When
        Integer result = calculator.Add(number);
        // Then
        assertEquals(0, result);
    }

    @Test
    public void shouldAddNumbers() {
        // Given
        String number = "77,1";
        // When
        Integer result = calculator.Add(number);
        // Then
        assertEquals(78, result);
    }

    @Test
    public void shouldAddOneNumbers() {
        // Given
        String number = "7";
        // When
        Integer result = calculator.Add(number);
        // Then
        assertEquals(7, result);
    }

    // Step 2 Test cases
    @Test
    public void shouldAddUnknownAmountOfNumbers() {
        // Given
        String number = "77,1,5,2";
        // When
        Integer result = calculator.Add(number);
        // Then
        assertEquals(85, result);
    }

    // Step 3 Test cases
    @Test
    public void shouldHandleNewLineDelimiter() {
        // Given
        String number = "1\n5,3";
        // When
        Integer result = calculator.Add(number);
        // Then
        assertEquals(9, result);
    }

    // Step 4 Test cases
    @Test
    public void shouldHandleDifferentDelimiter() {
        // Given
        String number = "//;\\n1;2";
        // When
        Integer result = calculator.Add(number);
        // Then
        assertEquals(3, result);
    }

    @Test
    public void shouldNotHandleNegatives() {
        // Given
        String number = "3,2,-4";
        // When
        // Then
        Throwable exception = assertThrows(RuntimeException.class, () -> calculator.Add(number));
        assertEquals("Negatives not allowed: [-4]", exception.getMessage());
    }
}