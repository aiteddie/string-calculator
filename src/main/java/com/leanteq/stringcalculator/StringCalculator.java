package com.leanteq.stringcalculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringCalculator {

    public int Add(String numbers) {
        List<String> negatives = new ArrayList<>();
        String delimiter = ",|\n";
        if (numbers == null || numbers.isEmpty()) return 0;
        if(numbers.startsWith("//")) {
            int delimiterIndex = numbers.indexOf("//") + 2;
            delimiter = numbers.substring(delimiterIndex, delimiterIndex + 1);
            numbers = numbers.substring(numbers.indexOf("n") + 1);
        }

        Integer result = Arrays.stream(numbers.split(delimiter))
                .map(element -> {
                            if (element.startsWith("-")) negatives.add(element);
                            return Integer.parseInt(element);
                        }
                ).reduce(0, Integer::sum);

        if (!negatives.isEmpty())
            throw new RuntimeException("Negatives not allowed: " + negatives);

        return result;
    }
}
